/**
 * Written by Phillip Hall, June 2014
 * A class for object House that has three attributes: 
 * wall color, type of floor, number of windows
 * @param houseColor String that stores the color of the house
 * @param floorType String that stores the type of flooring
 * @param numOfWindows Int that stores the number of windows
 */
public class House {
	
	static String houseColor, floorType;
	static int numOfWindows;

	//Constructor
	public House() {
		
	}
	
	/**Overloaded Constructor
	 * 
	 * @param color Assigns to houseColor
	 * @param floor Assigns to floorType
	 * @param windows Assigns to numOfWindows
	 */
	public House(String color, String floor, int windows) {
		setColor(color);
		setFloor(floor);
		setWindows(windows);
	}
	
	/**Prints out info about the House object
	 * 
	 */
	public void printHomeInfo() {
		System.out.println("Color: " + getColor() + "\nFloor Type: " + getFloor() + "\nNumber of Windows: " + getWindows() + "\n");
	}
	
	/**Assigns houseColor a value
	 * 
	 * @param color Sets houseColor equal to color
	 */
	public void setColor(String color) {
		houseColor = color;
	}
	
	/**Assigns floorType a value
	 * 
	 * @param floor Sets floorType equal to floor
	 */
	public void setFloor(String floor) {
		floorType = floor;
	}

	/**Assigns numOfWindows a value
	 * 
	 * @param windows Sets numOfWindows equal to windows
	 */
	public void setWindows(int windows) {
		numOfWindows = windows;
	}
	
	/**
	 * 
	 * @return houseColor
	 */
	public String getColor() {
		return houseColor;
	}
	
	/**
	 * 
	 * @return floorType
	 */
	public String getFloor() {
		return floorType;
	}

	/**
	 * 
	 * @return numOfWindows
	 */
	public int getWindows() {
		return numOfWindows;
	}
}

/**
 * 
 * @author Phillip Hall
 * Class used to register new users
 *
 */
public class Registration {

	public String usernames [] = new String[50];
	public String passwords [] = new String[50];
	int counter = 1;
	
	/**
	 * Compares username/password input against arrays storing usernames/passwords of valid accounts
	 * @param username Takes username as input
	 * @param password Takes password as input
	 */
	public void addNewUser(String username, String password) {
		usernames[0] = "Phil";
		passwords[0] = "Phil";
		usernames[counter] = username;
		passwords[counter] = password;
		counter++;
	}
}